var express = require('express');
var router = express.Router();

const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const url = process.env.URL;
const dbName = 'NoloNolo';
var multer = require('multer')
var path = require('path')
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})

var upload = multer({
  storage: storage
});

router.use(express.static('public'));

MongoClient.connect(url, function (err, db) {
  if (err) throw err;
  var dbo = db.db(dbName);

  //AGGIUNGI OGGETTO
  router.get('/aggiungiOggetto', async (req, res) => {
    if (req.session.user && req.session.user.type == "admin") {
      res.render('area-amministratore/AggiungiOggetto');
    } else {
      res.redirect('/login');
    }
  });

  //ELIMINA OGGETTO
  router.post('/eliminaOggetto:ID', async (req, res) => {
    if (req.session.user && req.session.user.type == "admin") {
      var id = new ObjectID(req.params.ID);
      await dbo.collection("Oggetti").deleteOne({
        _id: id
      }).then((data) => {
        res.redirect('/');
      }).catch((err) => {
        res.status(500).write(error);
      });
    } else {
      res.status(401).send("Operazione permessa solamente all'amministratore, se lo sei vai a effettuare il log-in");
    }

  });

  //AGGIUNGI OGGETTO
  router.post('/aggiungiOggetto', upload.single('foto'), async (req, res) => {
    if (req.session.user && req.session.user.type == "admin") {
      var ogg = req.body;
      await dbo.collection("Oggetti").insertOne({
        "tipo": ogg.tipo,
        "DisplayName": ogg.DisplayName,
        "Descrizione": ogg.Descrizione,
        "NomeCompleto": ogg.NomeCompleto,
        "Tariffa": parseInt(ogg.Tariffa),
        "Caratteristiche": ogg.Caratteristiche,
        "Avvertenze": ogg.Avvertenze,
        "Condizione": ogg.Condizione,
        "Stato": ogg.Stato,
        "Numero_Noleggi": parseInt(ogg.Numero_Noleggi),
        "Immagine": req.file.filename
      }).then(async () => {
        res.redirect('/');
      }).catch((error) => {
        console.log("c'e stato un errore nel modo in cui è stato memorizzato l'oggetto su mongoDB");
        res.status(500).write(error);
      });
    } else {
      res.status(401).send("Operazione permessa solamente all'amministratore, se lo sei vai a effettuare il log-in");
    }
  });


  //MODIFICA OGGETTO
  router.get('/modificaOggetto:ID', async (req, res) => {
    if (req.session.user && req.session.user.type == "admin") {
      var id = new ObjectID(req.params.ID);
      await dbo.collection("Oggetti").findOne({
        _id: id
      }).then((data) => {
        res.render('area-amministratore/ModificaOggetto', {
          object: data
        });
      }).catch((err) => {
        res.status(500).write(error);
      });
    } else {
      res.redirect('/');
    }
  });

  //MODIFICA OGGETTO
  router.post('/modificaOggetto:ID', async (req, res) => {
    if (req.session.user && req.session.user.type == "admin") {
      var id = new ObjectID(req.params.ID);
      var ogg = req.body;

      await dbo.collection("Oggetti").findOneAndReplace({
        _id: id
      }, {
        "tipo": ogg.tipo,
        "DisplayName": ogg.DisplayName,
        "Descrizione": ogg.Descrizione,
        "NomeCompleto": ogg.NomeCompleto,
        "Tariffa": parseInt(ogg.Tariffa),
        "Caratteristiche": ogg.Caratteristiche,
        "Avvertenze": ogg.Avvertenze,
        "Condizione": ogg.Condizione,
        "Stato": ogg.Stato,
        "Immagine": ogg.Immagine,
        "Numero_Noleggi": parseInt(ogg.Numero_Noleggi)
      }).then(async () => {
        res.redirect('/');
      }).catch((error) => {
        console.log("c'e stato un errore nel modo in cui è stato memorizzato l'oggetto su mongoDB");
        res.status(500).write(error);
      });
    } else {
      res.status(401).send("Operazione permessa solamente all'amministratore, se lo sei vai a effettuare il log-in");
    }
  });


  //devo stampare la lista di tutte le prenotazioni (passate, attive e future)
  router.get('/prenotazioni', async (req, res) => {
    if (req.session.user) {
      
      await dbo.collection("Prenotazioni").find({}).toArray().then((data) => {
        res.render("Prenotazioni", {
          Prenotazione: data
        });
      });

    } else {
      res.redirect('/login');
    }
  });

  router.get('/users', async (req, res) => {
    if (req.session.user && req.session.user.type == "admin") {
      await dbo.collection("Utenti").find({}).toArray().then((data) => {
        res.render('area-amministratore/Utenti', {
          session: req.session.user,
          numero_carrello: 0, 
          user: data
        });
      });
    } else {
      res.redirect('/login');
    }
  });

  //MODIFICA DATI
  router.get('/modificaDati:ID', async (req, res) => {
    if (req.session.user && req.session.user.type == "admin") {
      var id = new ObjectID(req.params.ID);

      // cerco le informazioni di questo id_Utente
      var session = await dbo.collection("Utenti").findOne({
        _id: id
      });
      var dati_utili = {
        success: "",
        mostra: ""
      }

      res.render('area-amministratore/ModificaDati', {
        user: session,
        util: dati_utili
      });
    } else {
      res.redirect("/login");
    }
  });

  //MODIFICA DATI
  router.post('/modificaDati:ID', async (req, res) => {
    if (req.session.user && req.session.user.type == "admin") {
      var id = new ObjectID(req.params.ID);
      var document = doc1 = Object.assign({}, req.body);

      // cerco le informazioni di questo id_Utente
      var utente = await dbo.collection("Utenti").findOne({
        _id: id
      });

      //aggiorno la variabile di sessione effettivamente
      for (let i in utente) {
        if (document[i]) { // se il valore ricevuto è non nullo allora modifico la variabile di sessione e lo stampo
          if (document[i] == 'CaNcElLaTo-9999') {
            document[i] = "";
          }
          utente[i] = document[i];
        }
      }
      // creao una copia della varibile di sessione senza il parametro _id
      for (let x in utente) {
        if (x != "_id") {
          doc1[x] = utente[x];
        }
      }

      var myquery = {
        _id: id
      };
      var newvalues = {
        $set: doc1
      };
      var dati_utili = {
        success: "cambio dati avvenuto con successo",
        mostra: ""
      }
      dbo.collection("Utenti").updateOne(myquery, newvalues).then(async () => {
        res.render('area-amministratore/ModificaDati', {
          user: utente,
          util: dati_utili
        });
      });

    } else {
      res.status(401).send("per effettuare questa operazione bisogna essere loggati");
    }
  });

  //ELIMINA UTENTE
  router.post('/eliminaUtente:ID', async (req, res) => {

    if (req.session.user && req.session.user.type == "admin") {
      var id = new ObjectID(req.params.ID);
      await dbo.collection("Utenti").deleteOne({
        _id: id
      }).then((data) => {
        res.redirect('/admin/users');
      }).catch((err) => {
        res.status(500).write(error);
      });
    } else {
      res.status(401).send("Operazione permessa solamente all'amministratore, se lo sei vai a effettuare il log-in");
    }

  });

  //CREA UTENTE
  router.get('/creaUtente', async (req, res) => {
    res.render('area-amministratore/creaUtente', {
      errore: ""
    });
  });

  //CREA UTENTE
  router.post('/creaUtente', async (req, res) => {
    var document = req.body;
    var email = document.email;
    var emailDB = "";

    await dbo.collection("Utenti").findOne({
        email: email
      })
      .then(async (data) => {
        emailDB = data.email;
      }).catch((err) => {
        console.log('\n ce stato un errore nel find (mondoDB)\n');
      });

    if (email === emailDB) {
      res.render('creaUtente', {
        errore: "email gia presente nel DB"
      });
    } else {
      await dbo.collection("Utenti").insertOne(document)
        .then(async () => {
          res.redirect('/admin/users');
        })
        .catch((err) => {
          console.log('si è verificato un errore nell inserimento del documento (inertone -- riga 43)')
        });
    }
  });

  //VISUALIZZA NOLEGGI
  router.get('/visualizzaNoleggi:ID', async (req, res) => {
    if (req.session.user && req.session.user.type == "admin") {
      var id = new ObjectID(req.params.ID);
      await dbo.collection("Noleggi").aggregate(
        [
          {
            '$lookup': {
              'from': 'Oggetti', 
              'let': {
                'tmp': {
                  '$toObjectId': '$id_Oggetto'
                }
              }, 
              'pipeline': [
                {
                  '$match': {
                    '$expr': {
                      '$eq': [
                        '$_id', '$$tmp'
                      ]
                    }
                  }
                }, {
                  '$project': {
                    'Tariffa': 1,
                    'Immagine' : 1
                  }
                }
              ], 
              'as': 'Dati_Oggetto'
            }
          }, {
            '$unwind': {
              'path': '$Dati_Oggetto', 
              'preserveNullAndEmptyArrays': true
            }
          }, {
            '$lookup': {
              'from': 'Noleggi', 
              'let': {
                'tmp': {
                  '$toObjectId': '$id_Oggetto'
                }
              }, 
              'pipeline': [
                {
                  '$match': {
                    '$expr': {
                      '$eq': [
                        {
                          '$toObjectId': '$id_Oggetto'
                        }, '$$tmp'
                      ]
                    }
                  }
                }, {
                  '$project': {
                    '_id': 1, 
                    'DataInizio': 1, 
                    'DataFine': 1
                  }
                }
              ], 
              'as': 'Noleggi'
            }
          }
        ]
      ).toArray().then(async(data) => {
        var prenotazione = await dbo.collection("Prenotazioni").findOne({
          _id : id
        })
        res.render('area-amministratore/visualizzaNoleggi', {
          nol: data,
          prenotazione : prenotazione
        });
      }).catch((error) => {
        res.render("Home");
        res.status(500).write(error);
    });
    }
  });

  //MODIFICA NOLEGGIO
    router.post('/modificaNoleggio:ID', async (req, res) => {
      if (req.session.user) {
        var ogg = req.body;
        let status;
        let oggi = new Date();
        let DataI =  new Date(ogg.DataInizio);
        let DataF =  new Date(ogg.DataFine);
        if( (oggi <= DataF) && (oggi >= DataI)){
          status = "attivo";
        }else if(oggi > DataF){
          status = "chiuso";
        }else if(oggi < DataI){
          status = "futuro";
        }
        var id = new ObjectID(req.params.ID);       
  
        var idPrenotazione = new ObjectID(ogg.Id_Prenotazione);
        var idUtente = new ObjectID(ogg.id_Utente);
        var dataInizio = DataI;
        var dataFine = DataF;
        var newTariffa = ogg.Tariffa_Prenotazione - ogg.Tariffa_Vecchia + parseInt(ogg.Tariffa_Totale);
        var Codice_Prenotazione = parseInt(ogg.Codice_Prenotazione);

        if (dataInizio < dataFine) { 
          await dbo.collection("Noleggi").findOneAndReplace({
            _id: id
          }, {
            "DataInizio": dataInizio.addHours(15),
            "DataFine": dataFine.addHours(24),
            "Nome" : ogg.Nome,
            "Note": ogg.Note,
            "id_Oggetto": ogg.id_Oggetto,
            "id_Utente": idUtente,
            "Tariffa_Totale": parseInt(ogg.Tariffa_Totale),
            "Status" : status
          }).then(async () => {
            await dbo.collection("Prenotazioni").updateOne({ _id : idPrenotazione},{ $set: {"Tariffa_Totale" : newTariffa }}).then(async () => {
              await dbo.collection("Fatture").updateOne({ Codice : Codice_Prenotazione},{ $set: {"Totale" : newTariffa }}).then(async () => {
              res.redirect('/');
            }).catch((error) => {
              }).catch((error) => {
              }).catch((error) => {
                console.log("c'e stato un errore nel modo in cui è stato memorizzato l'oggetto su mongoDB");
                res.status(500).write(error);
             });
            });
            });
        } else {
          res.send('Prenotazione non valida!Torna alla home http://localhost:3000/');
        }
      } else {
        res.status(401).send();
      }
      
      
    });

    //LISTA NOLEGGI
    router.get('/listaNoleggi', async (req, res) => {
      if (req.session.user && req.session.user.type == "admin") {
        
        await dbo.collection("Noleggi").aggregate(
          [
            {
              '$lookup': {
                'from': 'Utenti', 
                'localField': 'id_Utente', 
                'foreignField': '_id', 
                'as': 'dati_Utente'
              }
            }, {
              '$unwind': {
                'path': '$dati_Utente', 
                'preserveNullAndEmptyArrays': true
              }
            }
          ]
        ).toArray().then((dataNoleggi) => {
           res.render("area-amministratore/listaNoleggi", {
            noleggio: dataNoleggi
          });
        }).catch(error=>{
          console.log("Aggregate : Error: "+error);
        })
      } else {
        res.redirect('/login');
      }
    });

    //CHIUDI NOLEGGIO
    router.get("/chiudiNoleggio:ID", async(req, res)=>{
      var id = new ObjectID(req.params.ID);
      if (req.session.user && req.session.user.type == "admin") {
        await dbo.collection("Noleggi").aggregate(
          [
            {
              '$match': {
                '_id': id
              }
            }, {
              '$lookup': {
                'from': 'Utenti', 
                'localField': 'id_Utente', 
                'foreignField': '_id', 
                'as': 'dati_Utente'
              }
            }, {
              '$unwind': {
                'path': '$dati_Utente', 
                'preserveNullAndEmptyArrays': true
              }
            }
          ]
        ).toArray().then((dataNoleggi) => {
           res.render("area-amministratore/chiudiNoleggio", {
            noleggio: dataNoleggi
          });
        }).catch(error=>{
          console.log("Aggregate : Error: "+error);
        })
      } else {
        res.redirect('/login');
      }
    });

    //FATTURA CHIUSURA NOLEGGIO
    router.post("/fatturaChiusuraNoleggio", async (req, res) => {
      var tariffa = req.body.tariffa; 
      var noleggio = JSON.parse(req.body.noleggio); 
      var penale = [{"Nome":"ritardo", "Tariffa": tariffa}];
      var id = new ObjectID(noleggio._id);
      
      if (req.session.user && req.session.user.type == "admin") {
          var dataOggi = new Date();
          var codice = Math.floor(Math.random() * 1001);
          
          await dbo.collection("Fatture").insertOne({
            "nome_utente": noleggio.dati_Utente.nome,
            "cognome_utente": noleggio.dati_Utente.cognome,
            "indirizzo" : noleggio.dati_Utente.indirizzo,
            "citta" : noleggio.dati_Utente.citta,
            "cap" : noleggio.dati_Utente.cap,
            "email_utente" : noleggio.dati_Utente.email,
            "totale" :parseInt(tariffa),
            "data" : dataOggi, 
            "oggetti" : penale[0],
            "codice" : parseInt(codice), 
            "id_noleggio": noleggio._id
          }).then(async (data) => {

            res.render('Fattura', {
              Utente: noleggio.dati_Utente,
              Tariffa_Totale :  tariffa,
              Noleggi :penale,
              Data : dataOggi,
              Codice : codice
            });
          }).catch(error => {
            console.log("Errore nell'inserimento della fattura: "+ error)
          })

          // cambiare lo status del noleggio
          await dbo.collection("Noleggi").updateOne({_id: id}, { $set: { Status: "Interrotto" }})
            .then(d=>{
            }).catch(error=>{ 
              console.log("Errore nel update di status di un  noleggio"+error)
            });

        } else {
          res.redirect('/login');
        }
      
    });

    //VISUALIZZA FATTURA CHIUSURA
    router.get("/visualizzaFatturaChiusura:ID", async (req, res)=>{
      if (req.session.user && req.session.user.type == "admin") {
        await dbo.collection("Fatture").findOne({id_noleggio: req.params.ID})
          .then(fattura=>{
            var datiUtente = {
              "nome": fattura.nome_utente , 
              "cognome": fattura.cognome_utente  , 
              "indirizzo": fattura.indirizzo  , 
              "citta":fattura.citta   , 
              "cap": fattura.cap  , 
              "email": fattura.email_utente
            }

            var penale = [{"Nome":"Penale", "Tariffa": fattura.oggetti.Tariffa}];

            res.render('Fattura', {
              Utente: datiUtente,
              Tariffa_Totale :  fattura.oggetti.Tariffa,
              Noleggi : penale ,
              Data : fattura.data,
              Codice : fattura.codice
            });

          }).catch(error =>{ 
            console.log("Errore nel cercare le fattura di chiusura noleggio :"+error)
          });

      } else { 
        res.redirect("/login");
      }
    });

    //ELIMINA PRENOTAZIONI
    router.post('/eliminaPrenotazione:ID', async (req,res) => {
      if(req.session.user){
        var id = new ObjectID(req.params.ID);
        var Noleggi = [];           
        await dbo.collection("Prenotazioni").findOne({_id : id}).then(async (dataNoleggi) => {
          for(var i = 0; i<dataNoleggi.Noleggi.length; i++){
            var tmp = new ObjectID(dataNoleggi.Noleggi[i]);
            Noleggi.push(tmp);
          }
          const myquery2 = {
            $or: Noleggi.map(item => {
              return {
                _id: item
              }
            })
          }
          await dbo.collection("Noleggi").deleteMany(myquery2).then(async (dataNoleggi2) => {
            await dbo.collection("Prenotazioni").deleteOne({
              _id: id
            }).then((data) => {
              res.redirect('/');
            }).catch((error) => {});
            }).catch((error) => {});
            }).catch((error) => {
                  res.status(500).write(error); 
            c});    
      }else{
        res.render('login');
     }
    });
     
    //STATISTICHE
    router.get("/statistiche", async (req, res)=>{
      if (req.session.user && req.session.user.type == "admin") {
        
        // top 3 Ogetti piu noleggiati
        var top3OggettiNOleggiati; 
        
        await dbo.collection("Oggetti").find().sort({Numero_Noleggi: -1}).limit(3).toArray().then((data)=>{         
          top3OggettiNOleggiati = data; 
        }).catch((err)=>{
          console.log("Errore top 3 ogggetti piu noleggiati(DB): "+ err);
        });
        
        var top3NoleggioCostosi ; 
        await dbo.collection("Noleggi").find().sort({Tariffa_Totale: -1}).limit(3).toArray().then((data)=>{
          top3NoleggioCostosi = data; 
        }).catch((err)=>{
          console.log("Errore top 3 noleggi piu costosi(DB): "+ err);
        });

        var top3UtentiSpendaccioni; 
        await dbo.collection("Noleggi").aggregate(
          [
            {
              '$group': {
                '_id': '$id_Utente', 
                'sommaPagata': {
                  '$sum': '$Tariffa_Totale'
                }
              }
            }, {
              '$unwind': {
                'path': '$_id', 
                'preserveNullAndEmptyArrays': false
              }
            }, {
              '$lookup': {
                'from': 'Utenti', 
                'localField': '_id', 
                'foreignField': '_id', 
                'as': 'utente'
              }
            }, {
              '$unwind': {
                'path': '$utente', 
                'preserveNullAndEmptyArrays': false
              }
            }, {
              '$sort': {
                'sommaPagata': -1
              }
            }, {
              '$limit': 3
            }
          ]
        ).toArray()
        .then((data)=>{ top3UtentiSpendaccioni = data})
        .catch(err => console.log("Errore top3UtentiSpendaccioni : "+ err));

        var top3UtentiNoleggi;
        await dbo.collection("Noleggi").aggregate(
          [
            {
              '$group': {
                '_id': '$id_Utente', 
                'numeroNoleggi': {
                  '$sum': 1
                }
              }
            }, {
              '$lookup': {
                'from': 'Utenti', 
                'localField': '_id', 
                'foreignField': '_id', 
                'as': 'utente'
              }
            }, {
              '$unwind': {
                'path': '$utente', 
                'preserveNullAndEmptyArrays': false
              }
            }, {
              '$sort': {
                'numeroNoleggi': -1
              }
            }, {
              '$limit': 3
            }
          ]
        ).toArray()
        .then((data)=>{
          top3UtentiNoleggi = data; 
        }).catch(err=> console.log("Errore top3UtentiNoleggi :"+err));

        var top3OggettiRemunerativi; 
        await dbo.collection("Noleggi").aggregate(
          [
            {
              '$group': {
                '_id': '$id_Oggetto', 
                'sommaOttenuta': {
                  '$sum': '$Tariffa_Totale'
                }
              }
            }, {
              '$lookup': {
                'from': 'Oggetti', 
                'localField': '_id', 
                'foreignField': '_id', 
                'as': 'oggetto'
              }
            }, {
              '$unwind': {
                'path': '$oggetto', 
                'preserveNullAndEmptyArrays': false
              }
            }, {
              '$sort': {
                'sommaOttenuta': -1
              }
            }, {
              '$limit': 3
            }
          ]
        ).toArray()
        .then(data=>{
          top3OggettiRemunerativi = data; 
        }).catch(err => console.log("Errore top3 oggetti piu remunerativi : "+err));

       

        res.render("area-amministratore/statistiche", { 
          top3OggettiNOleggiati : top3OggettiNOleggiati, 
          top3NoleggioCostosi : top3NoleggioCostosi, 
          top3UtentiSpendaccioni : top3UtentiSpendaccioni, 
          top3UtentiNoleggi : top3UtentiNoleggi
        });
       } else { 
        res.redirect("/login");
      }
    });

    router.get("/visualizzaFatturaPrenotazione:ID", async (req, res)=>{
      var codice_fattura = parseInt(req.params.ID);
      var Noleggi = [];
      if (req.session.user) {
        await dbo.collection("Fatture").findOne({Codice: codice_fattura})
          .then(async (fattura) => { 
            for(var i = 0; i<fattura.Oggetti.length; i++){
              var tmp = new ObjectID(fattura.Oggetti[i]);
              Noleggi.push(tmp);
            }
            const myquery3 = {
              $or: Noleggi.map(item => {
                return {
                  _id: item
                }
              })
            }
            
            await dbo.collection("Noleggi").find(myquery3).toArray().then(async (data) => {
              res.render('FatturaRiepilogo', {
                objects : fattura,
                noleggi : data
              });




          }).catch(error =>{ 
          }).catch(error =>{ 
            console.log("Errore nel cercare le fattura di chiusura noleggio :"+error)
          });
        });

      }else { 
        res.redirect("/login");
      }
    });
});

module.exports = router;