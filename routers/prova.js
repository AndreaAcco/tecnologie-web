var express = require('express');
var router = express.Router();

const MongoClient = require('mongodb').MongoClient;
const url = process.env.URL;
const dbName = 'NoloNolo';
const nodemailer = require('nodemailer');
router.use(express.static('public'));


MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db(dbName);

    router.get('/file-prova-sessione', async (req, res) => {
        if (!req.session.user) {
            //la sessione non esiste
            res.redirect('/');
        } else {
            // esiste una variabile di sessione
            res.render('area-utente/file_prova_sessione', {
                email: req.session.user.email
            });
        }
    });

    router.get('/navbar', async (req, res)=>{
        var num_elementi_carrello = 0;
        if (req.session.user) {
           
            num_elementi_carrello = await dbo.collection("Carrello").count({id_utente: req.session.user._id});
        }
        res.render('area-utente/provaHeader', {
            session  : req.session.user , 
            nome_utente:req.session.nome,
            numero_carrello : num_elementi_carrello
        });
    });

    router.post("/email", async (req, res)=>{
        var emailReceiver = req.body.email;
        var heading = req.body.heading;
        var text = "<h1>"+req.body.text+"</h1>";
       
        var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'nicolo.sguerso@gmail.com',
            pass: 'viva-le-1999'
        }
        });

        var mailOptions = {
        from: 'nicolo.sguerso@gmail.com',
        to:  emailReceiver,
        subject: heading,
        html: text
        };

        transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } 
        });
        res.redirect("/prova/file-prova-sessione");
    });
});


module.exports = router;