var express = require('express');
var router = express.Router();

const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const url = process.env.URL;
const dbName = 'NoloNolo';
const nodemailer = require('nodemailer');

router.use(express.static('public'));

MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db(dbName);

    router.get('/registrazione', async (req, res) => {
        res.render('registrazione', {errore:""});
    });

    router.post('/registrazione', async (req, res) => {
        var document = req.body;
        // devo controllare che l'email inserita non sia gia presente nel db
        // se lo è devo reindirizzare la pagina con una scritta d'errore relativa alla email
        var email = document.email;
        var emailDB ="";

        await dbo.collection("Utenti").findOne({email: email})
           .then(async (data)=>{
              emailDB = data.email;
           }).catch((err)=>{
             console.log('\n ce stato un errore nel find (mondoDB)\n');
            });
        
        if(email === emailDB){
          res.render('registrazione',{errore :"email gia presente nel DB"});
        }else{
          await dbo.collection("Utenti").insertOne(document)
            .then(async()=>{
              res.redirect('/');
            })
            .catch((err)=>{
              console.log('si è verificato un errore nell inserimento del documento (inertone -- riga 43)')
            });
        }
    });


    router.get('/modificaDati', async (req, res)=>{
      if(req.session.user){
        var num_elementi_carrello = 0;
        num_elementi_carrello = await dbo.collection("Carrello").count({id_utente: req.session.user._id});
          res.render('area-utente/ModificaDati', {session : req.session.user, successo:"", numero_carrello: num_elementi_carrello});
      }else{
        res.redirect('/login');
      }
    });


    router.post('/modificaDati', async (req, res)=>{
     if(req.session.user){
      var id = new ObjectID(req.session.user._id);
      var document = doc1 = Object.assign({}, req.body);
      num_elementi_carrello = await dbo.collection("Carrello").count({id_utente: req.session.user._id});
      //aggiorno la variabile di sessione effettivamente
      for(let i in req.session.user){
       if(document[i]){   // se il valore ricevuto è non nullo allora modifico la variabile di sessione e lo stampo
        if(document[i] == 'CaNcElLaTo-9999'){
         document[i]= "";
        }
        req.session.user[i] = document[i];
       }
      }
      // creao una copia della varibile di sessione senza il parametro _id
      for(let x in req.session.user){
        if(x != "_id"){
          doc1[x] = req.session.user[x];
        }
      }

      var myquery = { _id: id };
      var newvalues = { $set: doc1 };
      var num_elementi_carrello = 0;
      num_elementi_carrello = await dbo.collection("Carrello").count({id_utente: req.session.user._id});
      dbo.collection("Utenti").updateOne(myquery, newvalues).then(async ()=>{
        res.render('area-utente/ModificaDati', {session : req.session.user, successo:"cambio dati avvenuto cons successo", numero_carrello : num_elementi_carrello});
      });
      
     }else{
      res.status(401).send("per effettuare questa operazione bisogna essere loggati");
     }
    });


    router.get('/prenotazione:ID', async (req, res) => {
      if(req.session.user){
        var num_elementi_carrello = 0;
        num_elementi_carrello = await dbo.collection("Carrello").count({id_utente: req.session.user._id});
        var id = new ObjectID(req.params.ID);
        await dbo.collection("Oggetti").aggregate(
        [
          {
            '$match': {
              '_id': id
            }
          }, {
            '$lookup': {
              'from': 'Noleggi', 
              'let': {
                'tmp': '$_id'
              }, 
              'pipeline': [
                {
                  '$match': {
                    '$expr': {
                      '$eq': [
                        {
                          '$toObjectId': '$id_Oggetto'
                        }, '$$tmp'
                      ]
                    }
                  }
                }, {
                  '$project': {
                    '_id': 1, 
                    'DataInizio': 1, 
                    'DataFine': 1
                  }
                }
              ], 
              'as': 'Noleggi'
            }
          }
        ]
        ).toArray().then((data) => {
          res.render('area-utente/Prenotazione', {
            object: data, 
            numero_carrello : num_elementi_carrello
          });
      }).catch((error) => {
          res.render("Home");
          res.status(500).write(error);
      });
    }else{
      res.redirect('/login');
    }
    });


    router.post('/completaPrenotazione', async (req, res) => {
      if(req.session.user){
        var document = req.body;
        var oggi = new Date();
        var myobj = [];
        for(let i = 0; i <document.carrello.length; i++){
          let status;
          // calcolo lo status di una prenotazione , 
          // if DataInizio < Oggi < DataFine -> attivo di
          // if oggi >DataFine -> chiuso -> casistica possibile solo tramite azione di retrodatare
          // if oggi < Data Inizio -> futuro
          let DataI = new Date(document.carrello[i].DataInizio);
          let DataF = new Date(document.carrello[i].DataFine).addHours(22);
          if( (oggi <= DataF) && (oggi >= DataI)){
            status = "attivo";
          }else if(oggi > DataF){
            status = "chiuso";
          }else if(oggi < DataI){
            status = "futuro";
          }
          var Nome = document.carrello[i].Nome;
          var DataInizio = DataI;
          var DataFine = DataF;
          var Note = document.carrello[i].Note;
          var id_Oggetto = ObjectID(document.carrello[i].id_Oggetto);
          var id_Utente = ObjectID(req.session.user._id);
          var Tariffa_Totale = parseInt(document.carrello[i].Tariffa);
          var Status = status;// chiuso - futuro - ritardo
          tmp = {Nome,DataInizio,DataFine,Note,id_Oggetto,id_Utente,Tariffa_Totale, Status}
          myobj.push(tmp);
        }

          await dbo.collection("Noleggi").insertMany(myobj).then(async (dataNoleggi) => {
            var tmp = [];
            for(let y = 0; y <document.carrello.length; y++){
              var id = new ObjectID(document.carrello[y].id_Oggetto);
              tmp.push(id);
            }

            const myquery = {
              $or: tmp.map(item => {
                return {
                  _id: item
                }
              })
            }

            var newvalues = { $inc: { Numero_Noleggi: 1} };

            await dbo.collection("Oggetti").updateMany(myquery,newvalues).then(async (data) => {
              var tmp2 = [];
              for(let y = 0; y <document.carrello.length; y++){
                var id = new ObjectID(document.carrello[y].id);
                tmp2.push(id);
              }
              const myquery2 = {
                $or: tmp2.map(item => {
                  return {
                    _id: item
                  }
                })
              }
              await dbo.collection("Carrello").deleteMany(myquery2).then(async (data) => {
                var tmp3 = [];
                for(let k = 0; k<dataNoleggi.ops.length; k++){
                  var id = new ObjectID(dataNoleggi.ops[k]._id);
                  tmp3.push(id);
                }
                var dataOggi = new Date();
                var codice = Math.floor(Math.random() * 1001);
                  await dbo.collection("Prenotazioni").insertOne({
                    "Tariffa_Totale": parseInt(document.Tariffa_Totale),
                    "id_Utente": ObjectID(req.session.user._id),
                    "Nome": req.session.user.nome,
                    "Cognome": req.session.user.cognome,
                    "Indirizzo" : req.session.user.indirizzo,
                    "Città" : req.session.user.citta,
                    "Cap" : req.session.user.cap,
                    "Email" : req.session.user.email,
                    "Noleggi" : tmp3,
                    "Data" : dataOggi,
                    "Codice" : parseInt(codice)
                  })
                  .then(async (data) => {

                    await dbo.collection("Fatture").insertOne({
                      "Nome": req.session.user.nome,
                      "Cognome": req.session.user.cognome,
                      "Indirizzo" : req.session.user.indirizzo,
                      "Città" : req.session.user.citta,
                      "Cap" : req.session.user.cap,
                      "Email" : req.session.user.email,
                      "Totale" : parseInt(document.Tariffa_Totale),
                      "Data" : dataOggi, 
                      "Oggetti" : tmp3,
                      "Codice" : parseInt(codice)
                    }).then(async (data) => {
                    

                      var idUser = new ObjectID(req.session.user._id);
                      var countP ;
                      await dbo.collection("Prenotazioni").find({id_Utente : idUser}).count().then(d=> countP =d).catch(err => console.error(err));
                      if(countP > 3){
                        //inserisco il codice nel server , Codice = nomeUtente+3
                        let r = (Math.random() + 1).toString(36).substring(7);
                        var codiceSconto = r +"_3"; 
                        
                        //controllo che non sia già stato inserito questo codice promozionales 
                        await dbo.collection("CodiciPromozioni").find({codice: codiceSconto}).count().then(async (d)=>{
                          if(!(d >0)){
                            // inserisco il codice promozionale ed invio la mail 
                            await dbo.collection("CodiciPromozioni").insertOne({
                              codice: codiceSconto,
                              valido: true,
                              sconto: 3
                            }).then(async (d)=>{
                              // se l'user ha fatto più di 3 noleggi, allora ottiene uno sconto del 3% sul futuro acquisto
                              // il codice verrà mandato via email
                              var transporter = nodemailer.createTransport({
                                service: 'gmail',
                                auth: {
                                    user: 'iTuoiEroi@gmail.com',
                                    pass: 'ituoieroi-01'
                                }
                                });
                            
                                var mailOptions = {
                                  from: 'iTuoiEroi@gmail.com',
                                  to:  req.session.user.email,
                                  subject: "Codice Sconto for YourSuperHeros.com",
                                  html: `<h1>YourSuperHeros</h1>
                                  <h3>Codice Sconto !</h3>
                                  <h4>Congratulazioni`+req.session.user.nome+`!,  grazie alle tue prenotazioni ti vogliamo premiare dandati questo codice promozionale!</h4>
                                  
                                  <h4> Codice : `+codiceSconto+`</h4>
                                  
                                  <h5>Il codice ti offre lo sconto del 3% su tutti gli acquisti futuri!!!</h5>`
                                };
                            
                                transporter.sendMail(mailOptions, function(error, info){
                                if (error) {
                                    console.log(error);
                                } 
                                });
                          
                            }).catch(err=> console.error("Errore inserimento del codice sconto nel db : (user.js)linea 299 :"+err));
                          }
                        }).catch(err=> console.log("Errore nel contare il numoer di codici promozionali \n"+err));
                        
                      }
                   
                    res.render('Fattura', {
                      Utente: req.session.user,
                      Tariffa_Totale : parseInt(document.Tariffa_Totale),
                      Noleggi : document.carrello,
                      Data : dataOggi,
                      Codice : codice
                    });                    
                  }).catch((err) => {});
                }).catch((err) => {});
              }).catch((err) => {});
            }).catch((err) => {});
          }).catch((err) => {});
    }else{
      res.status(401).send("Per effettuare questa operazione si deve essere loggati");
    }
    });

      router.post('/prenotazione:ID', async (req, res) => {
        if(req.session.user){
          var document = req.body;
          var dataInizio = new Date(document.DataInizio);
          var dataFine = new Date(document.DataFine);
          if (dataInizio < dataFine) { 
            var dataCarrello = await dbo.collection("Carrello").find({id_Oggetto : document.id_Oggetto}).toArray();
            if(dataCarrello.length == 0){
              await dbo.collection("Carrello").insertOne(
                {
                  "id_utente" : req.session.user._id,
                  "DataInizio" : dataInizio.addHours(15),
                  "DataFine" : dataFine.addHours(24),
                  "Note" : document.Note,
                  "id_Oggetto" : document.id_Oggetto,
                  "Tariffa_Totale" : parseInt(document.Tariffa_Totale)
                }
                ).then(async (data) => {
                  res.redirect('/');
                }).catch((err) => {});
            } else {
              var count = 0;
                for(var i = 0; i<dataCarrello.length; i++){
                  if (dataInizio < dataCarrello[i].DataInizio) {
                    if(dataFine > dataCarrello[i].DataInizio){
                      count++;
                    } 
                  }else if(dataInizio < dataCarrello[i].DataFine){
                    count++;
                  }
              }
              if(count == 0) {
                  await dbo.collection("Carrello").insertOne(
                    {
                      "id_utente" : req.session.user._id,
                      "DataInizio" : dataInizio.addHours(15),
                      "DataFine" : dataFine.addHours(24),
                      "Note" : document.Note,
                      "id_Oggetto" : document.id_Oggetto,
                      "Tariffa_Totale" : parseInt(document.Tariffa_Totale)
                    }
                    ).then(async (data) => {
                      res.redirect('/');
                    }).catch((err) => {});
                }else {
                  res.send('Conflitto con altre prenotazioni nel carrello');
                }
              }  
        } else {
            res.send('Prenotazione non valida!Torna alla home http://localhost:3000/');
        }
      }else{
          res.status(401).send("Per effettuare questa operazione si deve essere loggati");
      }
      });
 
      router.get('/prenotazioni', async (req, res) => {
        if (req.session.user) {
          var num_elementi_carrello = 0;
          var id = new ObjectID(req.session.user._id);
          num_elementi_carrello = await dbo.collection("Carrello").count({id_utente: req.session.user._id});
          await dbo.collection("Prenotazioni").find({id_Utente: id}).toArray().then((data) => {
            res.render("area-utente/PrenotazioniUtente", {
              Prenotazione: data, 
              numero_carrello : num_elementi_carrello
            });
          });
    
        } else {
          res.redirect('/login');
        }
      });


    router.get('/carrello', async (req, res) => {
      if(req.session.user){
        var num_elementi_carrello = 0;
            num_elementi_carrello = await dbo.collection("Carrello").count({id_utente: req.session.user._id});
        var codicePromozione ;
        await dbo.collection("CodiciPromozioni").find().toArray().then(d => { 
          codicePromozione =d; 
        });
        await dbo.collection("Carrello").aggregate(
        [
          {
            '$match': {
              'id_utente': req.session.user._id
            }
          }
          ,{
            '$lookup': {
              'from': 'Oggetti', 
              'let': {
                'tmp': '$id_Oggetto'
              }, 
              'pipeline': [
                {
                  '$match': {
                    '$expr': {
                      '$eq': [
                        {
                          '$toObjectId': '$$tmp'
                        }, '$_id'
                      ]
                    }
                  }
                }, {
                  '$project': {
                    'DisplayName': 1, 
                    'Immagine' : 1,
                    '_id': 0
                  }
                }
              ], 
              'as': 'Dati_Oggetto'
            }
          }, {
            '$unwind': {
              'path': '$Dati_Oggetto', 
              'preserveNullAndEmptyArrays': true
            }
          }
        ]
        ).toArray().then((data) => {
          res.render('Carrello', {
            objects: data, 
            numero_carrello : num_elementi_carrello, 
            codicePromozione: codicePromozione
        });
      }).catch((error) => {
          res.render("Home");
          res.status(500).write(error);
      });
      } else {
        res.redirect('/login')
      }
    });


    router.post('/eliminaOggettoCarrello:ID', async (req,res) => {
      if(req.session.user){
        var id = new ObjectID(req.params.ID);
        await dbo.collection("Carrello").deleteOne({
          _id: id
        }).then((data) => {
          res.redirect('/');
        }).catch((err) => {
            res.status(500).write(error);
        });
      }else{
        res.render('login');
     }
    });


    router.get('/ModificaNoleggio:ID', async (req, res) => {
      if (req.session.user) {
        var id = new ObjectID(req.params.ID);
        await dbo.collection("Noleggi").aggregate(
          [
            {
              '$lookup': {
                'from': 'Oggetti', 
                'let': {
                  'tmp': {
                    '$toObjectId': '$id_Oggetto'
                  }
                }, 
                'pipeline': [
                  {
                    '$match': {
                      '$expr': {
                        '$eq': [
                          '$_id', '$$tmp'
                        ]
                      }
                    }
                  }, {
                    '$project': {
                      'Tariffa': 1,
                      'Immagine' : 1
                    }
                  }
                ], 
                'as': 'Dati_Oggetto'
              }
            }, {
              '$unwind': {
                'path': '$Dati_Oggetto', 
                'preserveNullAndEmptyArrays': true
              }
            }, {
              '$lookup': {
                'from': 'Noleggi', 
                'let': {
                  'tmp': {
                    '$toObjectId': '$id_Oggetto'
                  }
                }, 
                'pipeline': [
                  {
                    '$match': {
                      '$expr': {
                        '$eq': [
                          {
                            '$toObjectId': '$id_Oggetto'
                          }, '$$tmp'
                        ]
                      }
                    }
                  }, {
                    '$project': {
                      '_id': 1, 
                      'DataInizio': 1, 
                      'DataFine': 1,
                      'Status' : 1
                    }
                  }
                ], 
                'as': 'Noleggi'
              }
            }
          ]
        ).toArray().then(async(data) => {
          num_elementi_carrello = await dbo.collection("Carrello").count({id_utente: req.session.user._id});
          var prenotazione = await dbo.collection("Prenotazioni").findOne({
            _id : id
          })
          res.render('area-utente/ModificaNoleggio', {
            nol: data,
            prenotazione : prenotazione,
            numero_carrello : num_elementi_carrello
          });
        }).catch((error) => {
          res.render("Home");
          res.status(500).write(error);
      });
      }
    });


    router.post('/modificaNoleggio:ID', async (req, res) => {
      if (req.session.user) {
        var id = new ObjectID(req.params.ID);
        var ogg = req.body;
        var idPrenotazione = new ObjectID(ogg.Id_Prenotazione);
        var idUtente = new ObjectID(ogg.id_Utente);
        var Codice_Prenotazione = parseInt(ogg.Codice_Prenotazione);
        var newTariffa = ogg.Tariffa_Prenotazione - ogg.Tariffa_Vecchia + parseInt(ogg.Tariffa_Totale);
        var dataInizio = new Date(ogg.DataInizio);
        var dataFine = new Date(ogg.DataFine);
        if (dataInizio < dataFine) { 
          await dbo.collection("Noleggi").findOneAndReplace({
            _id: id
          }, {
            "DataInizio": dataInizio.addHours(15),
            "DataFine": dataFine.addHours(24),
            "Nome" : ogg.Nome,
            "Note": ogg.Note,
            "id_Oggetto": ogg.id_Oggetto,
            "id_Utente": idUtente,
            "Tariffa_Totale": parseInt(ogg.Tariffa_Totale),
            "Status" : ogg.Status
          }).then(async () => {
              await dbo.collection("Prenotazioni").updateOne({ _id : idPrenotazione},{ $set: {"Tariffa_Totale" : newTariffa }}).then(async () => {
                await dbo.collection("Fatture").updateOne({ Codice : Codice_Prenotazione},{ $set: {"Totale" : newTariffa }}).then(async () => {
                  res.redirect('/');
          }).catch((error) => {
          }).catch((error) => {
          }).catch((error) => {
            console.log("c'e stato un errore nel modo in cui è stato memorizzato l'oggetto su mongoDB");
            res.status(500).write(error);
          });
          });
        });
        } else {
          res.send('Prenotazione non valida!Torna alla home http://localhost:3000/');
        }
      } else {
        res.status(401).send();
      }
    });


    router.get("/visualizzaFatturaPrenotazione:ID", async (req, res)=>{
      var codice_fattura = parseInt(req.params.ID);
      var Noleggi = [];
      if (req.session.user) {
        await dbo.collection("Fatture").findOne({Codice: codice_fattura})
          .then(async (fattura) => { 
            for(var i = 0; i<fattura.Oggetti.length; i++){
              var tmp = new ObjectID(fattura.Oggetti[i]);
              Noleggi.push(tmp);
            }
            const myquery3 = {
              $or: Noleggi.map(item => {
                return {
                  _id: item
                }
              })
            }
            console.log(myquery3);
            
            await dbo.collection("Noleggi").find(myquery3).toArray().then(async (data) => {
              res.render('FatturaRiepilogo', {
                objects : fattura,
                noleggi : data
              });




          }).catch(error =>{ 
          }).catch(error =>{ 
            console.log("Errore nel cercare le fattura di chiusura noleggio :"+error)
          });
        });

      }else { 
        res.redirect("/login");
      }
    });


    router.post('/eliminaPrenotazione:ID', async (req,res) => {
      if(req.session.user){
        var id = new ObjectID(req.params.ID);
        var Noleggi = [];           
        await dbo.collection("Prenotazioni").findOne({_id : id}).then(async (dataNoleggi) => {
          for(var i = 0; i<dataNoleggi.Noleggi.length; i++){
            var tmp = new ObjectID(dataNoleggi.Noleggi[i]);
            Noleggi.push(tmp);
          }
          const myquery2 = {
            $or: Noleggi.map(item => {
              return {
                _id: item
              }
            })
          }
          await dbo.collection("Noleggi").deleteMany(myquery2).then(async (dataNoleggi2) => {
            await dbo.collection("Prenotazioni").deleteOne({
              _id: id
            }).then((data) => {
              res.redirect('/');
            }).catch((error) => {});
            }).catch((error) => {});
            }).catch((error) => {
                  res.status(500).write(error); 
            c});    
      }else{
        res.render('login');
     }
    });
});


module.exports = router;

Date.prototype.addHours= function(h){
  this.setHours(this.getHours()+h);
  return this;
}