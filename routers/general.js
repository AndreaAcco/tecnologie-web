var express = require('express');
var router = express.Router();

const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const url = process.env.URL;
const dbName = 'NoloNolo';

router.use(express.static('public'));

MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db(dbName);

    router.get('/', async (req, res) => {
        var num_elementi_carrello = 0;
        if (req.session.user) {
            num_elementi_carrello = await dbo.collection("Carrello").count({id_utente: req.session.user._id});
        }
        await dbo.collection("Oggetti").find({}).toArray().then((data) => {
            res.render('Home', {
                objects: data,
                session: req.session.user,
                numero_carrello: num_elementi_carrello
            });
        }).catch((error) => {
            res.status(500).write(error);
        });
    });


    //LOGIN
    router.get('/login', (req, res) => {
        res.render('LogIn', {fallito : 0});
    });

    router.post('/login', async (req, res) => {
        var email = req.body.email;
        var password = req.body.password;

        //controllo che questo utente esista effettivamente, altrimenti lo ri-indirizzo alla registrazione
        var utente = await dbo.collection('Utenti').findOne({
            email: email,
            password: password
        }, async function (err, user) {
            if (err) {
                console.log(err);
                return res.status(500).send();
            }
            if (!user) {
                // l'utente che sta facendo il login non esiste oppure sono sbagliate le credenziali  
                res.render('LogIn', {fallito : 1});
            } else {
                // esiste  l'utente che sta facendo il login
                req.session.user = user;
                res.redirect('/');
                res.status(200).end();
            }
        });
    });


    //LOGOUT
    router.get('/logout', (req, res) => {
        if(req.session.user){
            req.session.destroy();
            res.redirect('/login');
        }else{
            res.status(500).send();
        }
    });

    router.get('/session', (req, res)=>{
        if(req.session.user){
            res.send(req.session.user);
        }else{
            res.status(500).send({session : undefined});
        }
    });
});

module.exports = router;