function header(id, ) {
    
    

    var x = document.getElementById(id);
    x.innerHTML = content;
}

function header_admin(id){
    var content = `
    <header>
        <div class="uk-background-top-right uk-background-cover uk-panel uk-flex uk-flex-middle uk-flex-center" style="background-image: url('backgroundHome.jpg');">
            <a href="/"><img src="Marvel.png" style="margin: 40px;"></a>
        </div>
        <nav class="uk-navbar-container" uk-navbar>
            <div class = "container">
                <ul class="uk-navbar-nav  ">
                    <li class="uk-active"><a href="/"><h4>Home</h4></a></li>
                    
                        <li>
                            <a href=""><h4>Menù</h4></a>
                            <div class="uk-navbar-dropdown">
                                <ul class="uk-nav uk-navbar-dropdown-nav uk-flex-middle">
                                        <li><a href="/admin/aggiungiOggetto"><h5>Aggiungi Oggetto</h5></a></li>
                                        <li><a href="/admin/prenotazioni"><h5>Lista Prenotazioni</h5></a></li>
                                        <li><a href="/admin/listaNoleggi"><h5>Lista Noleggi</h5></a></li>
                                        <li><a href="/admin/users"><h5>Lista utenti</h5></a></li>
                                        <li><a href="/admin/creaUtente"><h5>Crea utente</h5></a></li>
                                        <li><a href="/admin/statistiche"><h5>Statistiche</h5></a></li>
                                </ul>
                            </div>
                        </li>
                


                    <div class="uk-navbar-right">
                        <ul class="uk-navbar-nav ">
                        
                                <li><a href="/logout"><h4><span uk-icon="icon: sign-out"></span> Logout</h4></a></li>
                        </ul>
                    </div>
                </ul>
            </div>
        </nav>
    </header>`;
    var x = document.getElementById(id);
    x.innerHTML = content;
}


function header_nolog(id){
    var content = `
    <header>
    <div class="uk-background-top-right uk-background-cover uk-panel uk-flex uk-flex-middle uk-flex-center" style="background-image: url('backgroundHome.jpg');">
        <a href="/"><img src="Marvel.png" style="margin: 40px;"></a>
    </div>
    <nav class="uk-navbar-container" uk-navbar>
        <div class = "container">
            <ul class="uk-navbar-nav  ">
                <li class="uk-active"><a href="/"><h4>Home</h4></a></li>
                <li><a href="/login"><h4>Login</h4></a></li>
                <li><a href="/user/registrazione"><h4>Registrazione</h4></a></li>
                <div class="uk-navbar-right">
                    <ul class="uk-navbar-nav ">
                            <li><a href="/user/carrello" ><h4><span uk-icon="icon: cart"></span> 0 Carrello</h4></a></li>
                    </ul>
                </div>
            </ul>
        </div>
    </nav>
</header>`;
    var x = document.getElementById(id);
    x.innerHTML = content;
}

function header_user(id, numero_carrello){
    var content = `
    <header>
    <div class="uk-background-top-right uk-background-cover uk-panel uk-flex uk-flex-middle uk-flex-center" style="background-image: url('backgroundHome.jpg');">
        <a href="/"><img src="Marvel.png" style="margin: 40px;"></a>
    </div>
    <nav class="uk-navbar-container" uk-navbar>
        <div class = "container">
            <ul class="uk-navbar-nav  ">
                <li class="uk-active"><a href="/"><h4>Home</h4></a></li>
                    <li>
                        <a href=""><h4>Menù</h4></a>
                        <div class="uk-navbar-dropdown">
                            <ul class="uk-nav uk-navbar-dropdown-nav uk-flex-middle">
                                    <li><a href="/user/prenotazioni"><h5>Prenotazioni</h5></a></li>
                                    <li><a href="/user/modificaDati"><h5>Modifica dati</h5></a></li>
                            </ul>
                        </div>
                    </li>
                <div class="uk-navbar-right">
                    <ul class="uk-navbar-nav ">
                            <li><a href="/user/carrello" ><h4><span uk-icon="icon: cart"></span><span> ` + numero_carrello + ` </span> Carrello</h4></a></li>
                            <li><a href="/logout"><h4><span uk-icon="icon: sign-out"></span> Logout</h4></a></li>
                    </ul>
                </div>
            </ul>
        </div>
    </nav>
</header>`;
 
    var x = document.getElementById(id);
    x.innerHTML = content;
}
