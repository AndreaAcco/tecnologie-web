function header(session, admin ,n_carr) {
    console.log("collegamento riuscito");
    console.log("speriamo sia definitiva");
    var components = `
        <header>
        <div class="uk-background-top-right uk-background-cover uk-panel uk-flex uk-flex-middle uk-flex-center" style="background-image: url('backgroundHome.jpg');">
            <a href="/"><img src="Marvel.png" style="margin: 40px;"></a>
        </div>
        <nav class="uk-navbar-container" uk-navbar>
            <div class="uk-navbar-left">
                <ul class="uk-navbar-nav uk-flex-middle ">
                    <li class="uk-active"><a href="/"><h4>Home</h4></a></li>
                    `;
    if (session) {
        components += `
                        <li>
                            <a href=""><h4><span uk-icon="icon: user"></span> ` + session.nome + ` </h4></a>
                            <div class="uk-navbar-dropdown">
                                <ul class="uk-nav uk-navbar-dropdown-nav"> `;
        if (session){
            if(admin) {
                components += `         <li><a href="/user/prenotazioni"><h5>Prenotazioni</h5></a></li>
                                        <li><a href="/user/prenotazioni"><h5>Prenotazioni</h5></a></li>
                                        <li><a href="/user/modificaDati"><h5>Modifica dati</h5></a></li>
                                        <li><a href="/user/carrello" style="color: black;"><h5><span uk-icon="icon: cart"></span><span>` + n_carr + `</span> Carrello</h5></a></li>
                             `;
            
            } else {
                components += `     <li><a href="/admin/aggiungiOggetto"><h5>Aggiungi Oggetto</h5></a></li>
                                    <li><a href="/admin/prenotazioni"><h5>Lista Prenotazioni</h5></a></li>
                                    
                                    `;
            }
        }
        components += `
                                </ul>
                            </div>
                        </li>  `;
    } else {
        components += `
                        <li><a href="/login"><h4>Login</h4></a></li>
                    <li><a href="/user/registrazione"><h4>Registrazione</h4></a></li>
                    `;
    }

    components += `
                </ul>
            </div>
                <div class="uk-navbar-right">
                    <ul class="uk-navbar-nav uk-flex-middle">  `;
    if (session) {
        if (admin) {
            components +=   `
                            <li><a href="/user/carrello" style="color: black;"><h4><span uk-icon="icon: cart"></span><span>` + n_carr + `</span> Carrello</h4></a></li>    
                            `;
        }
        components +=   `
                        <li><a href="/logout"><h4><span uk-icon="icon: sign-out"></span> Logout</h4></a></li>
                         `;
    }

    components += `
                    </ul>
                </div>
            </nav>
            </header>
        `;

    return components
}