var express = require('express');
var app = express();
require('dotenv').config();
var session = require('express-session');

const path = require('path');
const root = path.join(__dirname);


app.use(express.static('public'));
app.use(express.json());  
app.use(express.urlencoded({extended: true})); 
app.set('view engine', 'ejs');
app.use(session({secret:"prova di una chiave", resave: false, saveUninitialized:true}));

prova = require('./routers/prova.js');
admin = require('./routers/admin.js');
general  = require('./routers/general.js');
user = require('./routers/user.js');

app.use('/prova/', prova);
app.use('/admin/', admin);
app.use('', general);
app.use('/user/', user);


app.listen(3000, function(){
  console.log('sono sulla porta 3000');
});

// web app exposed to http://localhost:3000/


//myobj = { state: "Pending", story: event.idstory, user: event.iduser, date: (new Date()).toISOString() };
//db.collection("PendingPdfCreations").insertOne(myobj);  